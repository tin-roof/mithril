/* @jsx m */
import m from 'mithril';
import { Loader, LoaderTools } from './components/util/loader';
import { Landing } from './components/landing';

(async () => {
	// Mount static elements
	m.mount(loader, Loader);

	// Show loader
	LoaderTools.show();

	// Set App settings
	window.App = {
		hostname: window.location.hostname,
		API: {
			server: __API__,
			debug: __DEBUG__
		},
		user: {
			loggedIn: false,
		},
	};

	// Define the base of the app
	var root = document.querySelector('#app');

	// Handle routing operations
	m.route.prefix = '';
	m.route(root, '/', {
		'/': Landing,
	});
})();
