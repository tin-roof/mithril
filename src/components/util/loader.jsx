/* @jsx m */
import m from 'mithril';

// Methods for handling the loader
export const LoaderTools = {
	show: () => {
		document.querySelector('loader').classList.add('show');
	},
	hide: () => {
		document.querySelector('loader').classList.remove('show');
	},
};

// Handle the loader view
export const Loader = () => {
	return {
		view: () => {
			return (
				<div class="container">
					<div class="shape shape1"></div>
					<div class="shape shape2"></div>
					<div class="shape shape3"></div>
					<div class="shape shape4"></div>
				</div>
			);
		},
	};
};
