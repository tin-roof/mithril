/* @jsx m */
import m from 'mithril';

// Handle the loader view
export const Landing = () => {
	return {
		view: () => {
			return (
				<div class="container">
					landing now!
				</div>
			);
		},
	};
};